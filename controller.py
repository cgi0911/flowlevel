# --------------------------------------------------------------------------------
# controller.py
#
# Flow-level simulator for link utilization and flow table usage
# Class: Controller
# Class Description: A simulated OpenFlow controller
#
# Author: Kuan-yin Chen
# Mail: cgi0911@gmail.com
# NYU Polytechnic School of Engineering
# --------------------------------------------------------------------------------

import networkx as nx
import netaddr as na
import random
from math import *
from heapq import *
from time import *
import pprint as pp
from csv import DictReader
from config import *
import numpy as np

class Controller:
    "A simulated OpenFlow controller"

    def __init__(self, fn_nodes='./test.nodes.csv', fn_links='./test.links.csv'):
        """
        Constructor of Controller class.
        - fn_nodes is the filename for (switching) nodes filename for nodes file (in CSV format).
        - fn_links is that for links file.
        - The Controller class use a NetworkX.Graph() data structure, which is non-directional,
          to store topology information.
        """
        self.topo = nx.Graph()      # Topology graph - a graph for backbone switches and links
        self.hosts = {}             # Host list - a dict for hosts in the network
        self.flows = {}             # Flow List
        self.hh_flows = {}          # Heavy hitter flows
        self.new_hh_flows = {}      # New heavy hitter flows
        self.switches = {}          # Switches - mainly routing tables

        # Load nodes from file: fn_nodes
        nodesFile = open(fn_nodes, 'r')
        nodesReader = DictReader(nodesFile)
        for ent in nodesReader:
            # Silently discard node entry if node of the same name is already in topology
            if( not self.topo.has_node(ent['name']) ):
                name = ent['name']
                del ent['name']
                #ent['n_hosts'] = int( ent['n_hosts'] )
                ent['n_hosts'] = N_HOSTS_PER_SW
                #ent['table_size'] = int( ent['table_size'] )
                ent['table_size'] = TABLE_SIZE_PER_SW
                #ent['table_usage'] = 0              # Record # of entries
                ent['hosts'] = []
                self.topo.add_node(name)            # Add node to graph
                self.topo.node[name].update(ent)    # Add attribute to graph node

        # Load links from file: fn_links
        linksFile = open(fn_links, 'r')
        linksReader = DictReader(linksFile)
        for ent in linksReader:
            node1 = ent['node1']
            node2 = ent['node2']
            # Silently discard link entry if duplicates found.
            if ( not self.topo.has_edge(node1, node2) ):
                self.topo.add_edge(node1, node2)    # Add link to graph. Non-directional
                del ent['node1']
                del ent['node2']
                ent['bw'] = float( ent['bw'] )
                ent['delay'] = float( ent['delay'] )
                self.topo.edge[node1][node2].update(ent)

        # Create hosts and assign IPs
        baseip = na.IPAddress('10.0.0.1')
        for n in self.topo.nodes():
            myIP = baseip
            self.topo.node[n]['baseip'] = baseip
            n_hosts = self.topo.node[n]['n_hosts']
            for i in range(n_hosts):
                self.hosts[myIP] = {}                       # Create a dict for new host
                self.hosts[myIP]['node'] = n                # Record the hosts's edge switch
                self.topo.node[n]['hosts'].append(myIP)     # Append the new host to its ingress node
                myIP = myIP + 1                             # Shift myIP by 1
            baseip = baseip + 2 ** int( ceil( log(n_hosts, 2) ) )   # Shift baseip by an entire segment
                                                                    # minimally containing the node's hosts.

        # Create switch routing tables
        for nd in self.topo.nodes():
            self.switches[nd] = {}      # Switch routing table stored in a dict




    def find_k_paths_yen(self, src, dst, k=3):
        """
        Find k paths using Yen's algorithm.
        """
        st_time = time()
        if (SHOW_K_PATH_CONSTRUCTION > 0):
            print "Finding %d paths from %s to %s" %(k, src, dst)

        confirmed_paths = []
        confirmed_paths.append( nx.shortest_path(self.topo, src, dst) )
        if (k <= 1):
            return confirmed_paths

        potential_paths = []

        for j in range(1, k):
            for i in range(0, len(confirmed_paths[j-1]) - 1):
                myTopo = nx.DiGraph(self.topo)      # Copy the topology graph
                myPath = confirmed_paths[j-1]
                spurNode = myPath[i]
                rootPath = myPath[0:i+1]

                l = len(rootPath)

                for p in confirmed_paths:
                    if (rootPath == p[0:l]):
                        if (myTopo.has_edge(p[l-1], p[l])):
                            myTopo.remove_edge(p[l-1], p[l])
                        else:
                            pass

                for q in rootPath[:-1]:
                    myTopo.remove_node(q)

                try:
                    spurPath = nx.shortest_path(myTopo, spurNode, dst)
                except:
                    spurPath = []

                if (not spurPath == []):
                    totalPath = rootPath + spurPath[1:]
                    potential_paths.append(totalPath)

            if (len(potential_paths) == 0):
                break

            potential_paths = sorted(potential_paths, key=lambda x: len(x) )
            confirmed_paths.append(potential_paths[0])
            potential_paths = []

        ed_time = time()
        if (SHOW_K_PATH_CONSTRUCTION > 0):
            print "%d-paths from %s to %s:" %(k, src, dst), confirmed_paths
            print "Time elapsed:", ed_time-st_time

        return confirmed_paths




    def find_k_paths_constrained(self, src, dst, k=3, relax=1):
        """
        Find at most k paths between src and dst.
        Constrained to paths no more than than min_dist+relax hops.
        - Relax: relax hop counts
        """
        min_dist = nx.shortest_path_length(self.topo, src, dst)
        pathHeap = []
        paths = []
        for myPath in nx.all_simple_paths(self.topo, src, dst):
            if ( len(myPath) > min_dist+relax):
                pass

            elif ( len(pathHeap) >= k ):
                if ( len(myPath) >= -1*pathHeap[0][0] ):
                    continue
                    # Don't bother to push in myPath if
                    # it is longer than longest path in queue
                else:
                    heappush( pathHeap, (-1*len(myPath), myPath) )
                    heappop(pathHeap)
            else:
                heappush(pathHeap, (-1*len(myPath), myPath) )

        for myTuple in pathHeap:
            paths.append(myTuple[1])

        return paths




    def find_k_paths_shortest_only(self, src, dst, k=3):
        """
        Find at most k paths between src and dst. Constrained to shortest paths.
        """
        paths = []
        for myPath in nx.all_shortest_paths(self.topo, src, dst):
            if ( len(paths) >= k):
                paths.append(myPath)
                paths = sorted(paths, key=lambda x: len(x), reverse=True)
                paths.pop(0)
            else:
                paths.append(myPath)

        return paths




    def setup_k_path_db(self, k=3, relax=1, method='shortest'):
        """
        Set up k-path database of all possible src-dst pairs.
        - k: maximum # of paths registered per src-dst pair.
        """

        self.k_path_db = {}

        for node1 in sorted(self.topo.nodes()):
            for node2 in sorted(self.topo.nodes()):
                if (node1 == node2):
                    continue
                else:
                    if (method == 'yen'):
                        self.k_path_db[(node1, node2)] = \
                            self.find_k_paths_yen(node1, node2, k=k)
                    elif (method == 'constrained'):
                        self.k_path_db[(node1, node2)] = \
                            self.find_k_paths_constrained(node1, node2, k=k, relax=relax)
                    else:
                        self.k_path_db[(node1, node2)] = \
                            self.find_k_paths_shortest_only(node1, node2, k=k)




    def get_path_objval(self, path):
        sum_objval = 0.0
        for mySW in path:
            table_size = self.topo.node[mySW]['table_size']
            usage = len(self.switches[mySW])
            if (not (table_size - usage)==0 ):
                objval = float(table_size) / (table_size-usage)
            else:
                return -1
            sum_objval += objval

        #final_objval = sum_objval / len(path)
        #final_objval = sum_objval / len(path)   # Avg. table util along the path
        final_objval = sum_objval

        return final_objval




    def path_sel_table_aware(self, src_node, dst_node):
        """
        Given source/dest node (switches), select a path with minimal table usage
        """
        avail_paths = self.k_path_db[(src_node, dst_node)]
        selPath = []                # If no path avaiable, return an empty path
        curr_objval = np.inf        # A very large number

        for myPath in avail_paths:
            objval = self.get_path_objval(myPath)
            if (objval == -1):
                continue            # Skip unavailable paths
            elif (objval < curr_objval):
                selPath = myPath
                curr_objval = objval

        return selPath




    def path_sel_random(self, src_node, dst_node):
        """
        Given source/dest node (switches), randomly select an available path.
        """
        avail_paths = []
        for pth in self.k_path_db[(src_node, dst_node)]:
            objval = sel.get_path_objval(pth)
            if (not objval == -1):
                avail_paths.append(pth)

        if (avail_paths == []):
            return []
        else:
            myPath = random.choice(avail_paths)
            return myPath




    def path_sel_shortest(self, src_node, dst_node):
        """
        Given source/dest node (switches), select the shortest path between.
        """
        myPath = nx.shortest_path(self.topo, src_node, dst_node)
        objval = self.get_path_objval(myPath)

        if (objval == -1):
            return []
        else:
            return myPath




    def switches_flow_inst(self, evTime, evDict_FlowInst):
        """
        Install flow entry to switches along the path
        """
        path = evDict_FlowInst['path']
        src_ip = evDict_FlowInst['src_ip']
        dst_ip = evDict_FlowInst['dst_ip']
        myFlow = (src_ip, dst_ip)
        self.flows[myFlow] = {}
        self.flows[myFlow]['path'] = path
        for i in range(len(path)):
            mySW = path[i]
            self.switches[mySW][myFlow] = {}
            self.switches[mySW][myFlow]['src_ip'] = src_ip
            self.switches[mySW][myFlow]['dst_ip'] = dst_ip

            if ( i == 0):
                self.switches[mySW][myFlow]['last_hop'] = src_ip
            else:
                self.switches[mySW][myFlow]['last_hop'] = path[i-1]

            if ( i == len(path)-1 ):
                self.switches[mySW][myFlow]['next_hop'] = dst_ip
            else:
                self.switches[mySW][myFlow]['next_hop'] = path[i+1]

            self.switches[mySW][myFlow]['byte_count'] = 0.0




    def switches_idle_timeout(self, evTime, evDict_IdleTimeout, flowgen):
        src_ip = evDict_IdleTimeout['src_ip']
        dst_ip = evDict_IdleTimeout['dst_ip']
        myFlow = (src_ip, dst_ip)
        path = self.flows[(src_ip, dst_ip)]['path']
        #path = flowgen.flows[(src_ip, dst_ip)]['path']
        for mySW in path:
            del self.switches[mySW][myFlow]

        del self.flows[myFlow]



    def print_nodes(self):
        """
        Print node names, and # of nodes, in the topology.
        """

        print "Switching nodes in the topology:"
        pp.pprint( self.topo.nodes() )
        print "Total %d nodes\n" %(len(self.topo.nodes() ) )




    def print_links(self):
        """
        Print link (tuple of node names), and # of links, in the topology.
        """

        print "Links in the topology:"
        pp.pprint( self.topo.edges() )
        print "Total %d links\n" %( len(self.topo.edges()) )




    def print_hosts(self):
        """
        Print hosts (a dictionary containing its attributes),
        and # of hosts, in the topology.
        """

        print "Hosts in the topology:"
        pp.pprint(self.hosts)
        print "Total %d hosts\n" %( len(self.hosts.keys() ) )
