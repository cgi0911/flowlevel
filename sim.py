#!/usr/bin/python

# --------------------------------------------------------------------------------
# sim.py
#
# Flow-level simulator for link utilization and flow table usage
#
# Author: Kuan-yin Chen
# Mail: cgi0911@gmail.com
# NYU Polytechnic School of Engineering
# --------------------------------------------------------------------------------

from controller import *
from flowgen import *
from switch import *
from flowgen import *
from heapq import *
from events import *
import pprint as pp
import numpy as np
from config import *    # System parameters
import os
import time




if __name__ == "__main__":
    exc_st_time = time.time()                   # Timer for execution time

    # Initialize the simulator
    sys_time = 0.0                              # System simulation timer

    ctrl = Controller(FN_NODES, FN_LINKS)       # Construct controller

    if (SHOW_PROGRESS > 0):
        print "Creating k-path database..."
    ctrl.setup_k_path_db(k=K_PATH, relax=RELAX, method=K_PATH_METHOD)
                                                # Initialize the k-path database

    #pp.pprint(ctrl.k_path_db)
    #quit()

    flowgen = FlowGen(ctrl)                     # Construct flow generator
    hh_reroute_time = HH_REROUTE_PERIOD - 1e-10 # Schedule HH report time

    # ----------------------------------------
    # Prepare log files
    # ----------------------------------------
    #node_list = sorted(ctrl.topo.nodes(), key=lambda x: int(x[1:]) )
    #link_list = sorted(ctrl.topo.edges(), key=lambda x: [int(x[0][1:]), int(x[1][1:])] )
    node_list = sorted(ctrl.topo.nodes() )
    link_list = sorted(ctrl.topo.edges() )
    # Link utilization stats collected in lists
    link_util_rmse_list = []
    link_util_avg_list = []
    link_util_min_list = []
    link_util_max_list = []
    link_util_q1_list = []
    link_util_q3_list = []
    link_util_q2_list = []
    throughput_list = []
    # Table utilization stats collected in lists
    table_util_rmse_list = []
    table_util_avg_list = []
    table_util_min_list = []
    table_util_max_list = []
    table_util_q1_list = []
    table_util_q3_list = []
    table_util_q2_list = []
    n_rec_pkt_in = 0        # # of received PacketIn requests
    n_rej_pkt_in = 0        # # of rejected PacketIn requests


    # Prepare log files and write header.
    if (not os.path.exists(DIR_LOG) ):
        os.mkdir(DIR_LOG)

    if (LOG_TABLE_UTIL > 0):
        t_file = open(FN_LOG_TABLE_UTIL_FULL, 'w')
        t_file.write('time,')
        for n in node_list:
            t_file.write('%s,' %(n))
        t_file.write('sw_avg,sw_rmse,min,max,q1,q3,median\n')

    if (LOG_LINK_UTIL > 0):
        l_file = open(FN_LOG_LINK_UTIL_FULL, 'w')
        l_file.write('time,')
        for l in link_list:
            l_file.write('%s,' %(l[0]+'_'+l[1]))
        l_file.write('link_avg,link_rmse,active_flows,total_flows,throughput,min,max,q1,q3,median\n')

    if (LOG_FLOW_STATS > 0):
        f_file = open(FN_LOG_FLOW_STATS_FULL, 'w')
        f_file.write('src_ip,dst_ip,src_node,dst_node,flow_id,maxbytes,rerouted,arr_time,st_time,ed_time,flow_time,avg_rate\n')

    if (LOG_PARAMS > 0):
        #print "config.py copied to %s" %(FN_PARAMS)
        log_params()

    if (LOG_SUMMARIES > 0):
        s_file = open(FN_SUMMARIES_FULL, 'a')


    # ----------------------------------------
    # Start simulation
    # ----------------------------------------

    # Preparation:
    events = []                                 # Event queue. A list acting as a min-heap, and
                                                # all operations on the heap are conducted using
                                                # heapq library. Each item in queue is a tuple (time, event).
                                                # events[0] is always the event with minimum time.
    events = flowgen.gen_init_flows(events)     # Accept initial flows from flow generator
    #heappush(events, handle_HHReport_event(0.0) )
                                                # Insert first HH report event

    while(sys_time + TIME_RES <= SIM_TIME):
        if (len(events) == 0):
            print "-> Event queue empty! Will terminate simulation."
            break


        else:
            while(len(events) <> 0 and gettime(events[0]) <= sys_time + TIME_RES):
                headEvent = heappop(events)
                evTime = gettime(headEvent)
                evDict = getdict(headEvent)
                evType = evDict['type']

                # Print just-popped event
                if(SHOW_EVENTS > 0):
                    print "-> %f sec: %s" %(evTime, evDict['type'])
                    pp.pprint(evDict, indent=4)

                # ------------------------------------------------------------
                # Event handling:
                # ------------------------------------------------------------

                # Type: 'FlowArrival'
                #   -> Schedule a 'PacketIn' event, SW_CTRL_DELAY from now
                if (evType == 'FlowArrival'):
                    # Schedule a packet in event, SW_CTRL_DELAY from now
                    heappush( events, handle_FlowArrival_event(evTime, evDict, flowgen) )
                    pass


                # Type: 'PacketIn'
                #   -> Schedule a 'FlowInstall' event, CTRL_SW_DELAY from now
                if (evType == 'PacketIn'):
                    n_rec_pkt_in += 1
                    new_evTime, new_evDict = handle_PacketIn_event(evTime, evDict, ctrl)
                    if (new_evDict['type'] == 'PacketIn'):  # Event 'PacketIn' received: the PacketIn is rejected
                        n_rej_pkt_in += 1
                    heappush( events, (new_evTime, new_evDict) )
                                            # If no path available, the event pushed
                                            # will be a delayed PacketIn.
                                            # If path available, the event will be a FlowInst

                    # if (new_evDict['path'] == []):
                    #     # Flow rejected due to lack of flow table space
                    #     # Silently discard the flow
                    #     del flowgen.flows[(evDict['src_ip'], evDict['dst_ip'])]
                    #     # Schedule a new flow
                    #     heappush( events, flowgen.gen_new_flow(evDict['src_ip'], evTime=evTime+REJECT_TIMEOUT) )
                    # else:
                    #     heappush( events, (new_evTime, new_evDict) )


                # Type: 'FlowInstall'
                #   ->
                if (evType == 'FlowInst'):
                    # Handle the flow installation at flowgen, switches, controller and links
                    inst_fail, new_evDict = handle_FlowInst_event(evTime, evDict, ctrl, flowgen, events)
                    if (inst_fail > 0):
                        n_rej_pkt_in += 1
                        #heappush( events, flowgen.gen_new_flow(evDict['src_ip'], evTime=evTime+REJECT_TIMEOUT) )
                        heappush(events, (evTime+REJECT_TIMEOUT, new_evDict) )




                # Type: 'FlowEnd'
                #   -> Schedule idle time out.
                if (evType == 'FlowEnd'):
                    # Schedule idle time out
                    heappush(events, handle_FlowEnd_event(evTime, evDict, ctrl, flowgen, f_file) )
                    # !!!TEMPORARY!!!: Schedule next flow arrival?
                    heappush( events, flowgen.gen_new_flow(evDict['src_ip'], evTime=evTime) )


                # Type: 'HardTimeout'
                #   -> Schedule a 'PacketIn' event, SW_CTRL_DELAY from now
                #if (evType == 'HardTimeout'):
                #    pass


                # Type: 'IdleTimeout'
                #   -> ????
                if (evType == 'IdleTimeout'):
                    handle_IdleTimeout_event(evTime, evDict, ctrl, flowgen)



                # Type: 'HHReport'
                #   -> Schedule next 'HHReport' event, HH_REPORT_PERIOD from now
                #if (evType == 'HHReport'):
                #    heappush(events, handle_HHReport_event(evTime, evDict) )


                # Type: 'UtilReport'
                #   -> ????
                #if (evType == 'TableUtilReport'):
                #    pass


        # At the end of time slot, update states and statistics of flows, switches & links
        # FlowEnd events will be handled in next slot
        sys_time += TIME_RES
        if (SHOW_PROGRESS > 0):
            print "-> %f sec" %(sys_time),

        # ----------------------------------------
        # Update statistics!!!
        # ----------------------------------------
        update_stats(sys_time, ctrl, flowgen, events)


        # ----------------------------------------
        # Log stats to files
        # ----------------------------------------
        if (LOG_LINK_UTIL > 0):
            l_file.write('%.6f,' %(sys_time))                       # time

            util_list = []
            for lk in link_list:
                myUtil = flowgen.links[lk]['util_in_slot']
                l_file.write('%.6f,' %(myUtil))                     # link utilizations
                util_list.append(myUtil)

            link_util_avg_list.append(np.mean(util_list))
            l_file.write('%.6f,' %(link_util_avg_list[-1]) )        # average

            link_util_rmse_list.append(rmse(util_list))
            l_file.write('%.6f,' %(link_util_rmse_list[-1] ) )      # rmse

            n_active_flows = len([fl for fl in flowgen.flows \
                                  if flowgen.flows[fl]['status'] == 'active' ])
            l_file.write('%d,' %(n_active_flows))                   # active flows
            l_file.write('%d,' %(len(flowgen.flows.keys() )))       # total flows

            if (SHOW_PROGRESS > 0):
                print n_active_flows

            throughput_list.append(sum([flowgen.flows[fl]['bytes_in_slot'] for fl in flowgen.flows]) / TIME_RES )
            l_file.write('%.6f,' %( throughput_list[-1] ) )          # throughput

            link_util_min_list.append(np.min(util_list))
            l_file.write('%.6f,' %( link_util_min_list[-1]) )       # min

            link_util_max_list.append(np.max(util_list))
            l_file.write('%.6f,' %( link_util_max_list[-1]) )       # max

            link_util_q1_list.append(np.percentile(util_list, 25))
            l_file.write('%.6f,' %( link_util_q1_list[-1]) )        # q1

            link_util_q3_list.append(np.percentile(util_list, 75))
            l_file.write('%.6f,' %( link_util_q3_list[-1]) )        # q1

            link_util_q2_list.append(np.percentile(util_list, 50))
            l_file.write('%.6f,' %( link_util_q2_list[-1]) )        # q2 (median)

            l_file.write('\n')                                      # EOL




        if (LOG_TABLE_UTIL > 0):
            t_file.write('%.6f,' %(sys_time))                       # time
            util_list = []
            for sw in node_list:
                cap = ctrl.topo.node[sw]['table_size']
                usage = len(ctrl.switches[sw])
                myUtil = float(usage)/float(cap)
                t_file.write('%.6f,' %(myUtil))                     # table utilizations
                util_list.append(myUtil)

            table_util_avg_list.append(np.mean(util_list))
            t_file.write('%.6f,' %(table_util_avg_list[-1]) )       # average

            table_util_rmse_list.append(rmse(util_list))
            t_file.write('%.6f,' %(table_util_rmse_list[-1]) )       # rmse

            table_util_min_list.append(np.min(util_list))
            t_file.write('%.6f,' %( table_util_min_list[-1]) )       # min

            table_util_max_list.append(np.max(util_list))
            t_file.write('%.6f,' %( table_util_max_list[-1]) )       # max

            table_util_q1_list.append(np.percentile(util_list, 25))
            t_file.write('%.6f,' %( table_util_q1_list[-1]) )        # q1

            table_util_q3_list.append(np.percentile(util_list, 75))
            t_file.write('%.6f,' %( table_util_q3_list[-1]) )        # q3

            table_util_q2_list.append(np.percentile(util_list, 50))
            t_file.write('%.6f,' %( table_util_q2_list[-1]) )        # q2 (median)

            t_file.write('\n')                                       # EOL



        # ----------------------------------------
        # Do reroute if turned on
        # ----------------------------------------
        if (sys_time >= hh_reroute_time and sys_time + TIME_RES <= SIM_TIME):
            if (DO_REROUTE > 0):
                do_reroute(evTime, ctrl, flowgen, events)
                hh_reroute_time += HH_REROUTE_PERIOD
                pass


    exc_ed_time = time.time()


    s_file.write("K_PATH=%d  DO_REROUTE=%d  TABLE_SIZE_PER_SW=%d  N_HOSTS_PER_SW=%d  MODE_PATH_SEL=%s\n"    \
                  %(K_PATH, DO_REROUTE, TABLE_SIZE_PER_SW, N_HOSTS_PER_SW, MODE_PATH_SEL  ) )

    s_file.write("Stats summary:\n")

    s_file.write("Mean link utilization average (steady state): %.10f\n" %(np.mean(link_util_avg_list[ELIM_SLOTS:])) )
    s_file.write("Mean link utilization rmse (steady state): %.10f\n" %(np.mean(link_util_rmse_list[ELIM_SLOTS:])) )
    s_file.write("Mean link utilization min (steady state): %.10f\n" %(np.mean(link_util_min_list[ELIM_SLOTS:])) )
    s_file.write("Mean link utilization max (steady state): %.10f\n" %(np.mean(link_util_max_list[ELIM_SLOTS:])) )
    s_file.write("Mean link utilization first quantile (steady state): %.10f\n" %(np.mean(link_util_q1_list[ELIM_SLOTS:])) )
    s_file.write("Mean link utilization third quantile (steady state): %.10f\n" %(np.mean(link_util_q3_list[ELIM_SLOTS:])) )
    s_file.write("Mean link utilization median (steady state): %.10f\n" %(np.mean(link_util_q2_list[ELIM_SLOTS:])) )

    s_file.write("Mean throughput (steady state): %.10f\n" %(np.mean(throughput_list[ELIM_SLOTS:])) )

    s_file.write("Mean table utilization average (steady state): %.10f\n" %(np.mean(table_util_avg_list[ELIM_SLOTS:])) )
    s_file.write("Mean table utilization rmse (steady state): %.10f\n" %(np.mean(table_util_rmse_list[ELIM_SLOTS:])) )
    s_file.write("Mean table utilization min (steady state): %.10f\n" %(np.mean(table_util_min_list[ELIM_SLOTS:])) )
    s_file.write("Mean table utilization max (steady state): %.10f\n" %(np.mean(table_util_max_list[ELIM_SLOTS:])) )
    s_file.write("Mean table utilization first quantile (steady state): %.10f\n" %(np.mean(table_util_q1_list[ELIM_SLOTS:])) )
    s_file.write("Mean table utilization third quantile (steady state): %.10f\n" %(np.mean(table_util_q3_list[ELIM_SLOTS:])) )
    s_file.write("Mean table utilization median (steady state): %.10f\n" %(np.mean(table_util_q2_list[ELIM_SLOTS:])) )
    s_file.write("Received PacketIn events: %d\n" %(n_rec_pkt_in) )
    s_file.write("Rejected PacketIn evnets: %d\n" %(n_rej_pkt_in) )
    s_file.write("Execution time: %.10f\n" %(exc_ed_time - exc_st_time) )
    s_file.write("\n\n\n")
