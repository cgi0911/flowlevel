# --------------------------------------------------------------------------------
# flowgen.py
#
# Flow-level simulator for link utilization and flow table usage
# Class: FlowGen
# Class Description: A traffic model for generating simulated flows.
#
# Author: Kuan-yin Chen
# Mail: cgi0911@gmail.com
# NYU Polytechnic School of Engineering
# --------------------------------------------------------------------------------

from config import *
from heapq import *
from time import *
from events import *
import numpy as np
import random

class FlowGen:
    "A simulated IPv4 flow generator"

    def __init__(self, ctrl):
        """
        Constructor of FlowGen class.
        - ctrl is the controller.
        """
        self.hosts = ctrl.hosts     # A dict to record all hosts
        self.flows = {}             # A dict to record all flows
        self.links = {}             # A dict to record link information
        self.flowid = 0             # Integer. Each generated flow has an unique flow ID.

        for lk in ctrl.topo.edges():
            self.links[lk] = {}
            self.links[lk]['flows'] = []
                                    # For each link in topology, initialize a list
                                    # to record active flows running on it.
            self.links[lk]['cap_in_slot'] = ctrl.topo.edge[lk[0]][lk[1]]['bw'] * TIME_RES
            self.links[lk]['bytes_in_slot'] = 0.0
            self.links[lk]['util_in_slot'] = 0.0

        # Random seeding
        np.random.seed( int(time()) )




    def gen_a_rand_dst_flow(self, src, evTime=0.0):
        """
        Generate a flow with source given, and will randomly pick a destination.
        """

        myHost = src
        myHost_node = self.hosts[myHost]['node']
        myEvent = {'type': 'FlowArrival', \
                   'src_ip': myHost, 'flowid': self.flowid}
        self.flowid += 1
        remote = ''
        remote_node = ''

        # Randomly pick a destination (not the same as myHost)
        # - Source node shall not be the same as dest (remote) node
        # - Shall not generate a flow that is existing in self.flows
        while True:
            remote = random.choice(self.hosts.keys() )
            remote_node = self.hosts[remote]['node']
            if (remote_node <> myHost_node and \
                not (myHost, remote) in self.flows):
                myEvent['dst_ip'] = remote
                break

        myEvent['src_node'] = myHost_node
        myEvent['dst_node'] = remote_node

        # Decide whether large or small
        mean_rate = 0.0
        stdev_rate = 0.0
        mean_maxbytes = 0.0
        stdev_maxbytes = 0.0

        coinflip = np.random.uniform()
        isLarge = 0
        if (coinflip <= PROB_LARGEFLOW):
            isLarge = 1
        else:
            isLarge = 0

        # Decide data rate
        rate = 0.0
        maxbytes =0.0

        if (isLarge > 0):
            rate = np.random.uniform(RATE_LARGEFLOW_MIN, RATE_LARGEFLOW_MAX)
            maxbytes = np.random.uniform(MAXBYTES_LARGEFLOW_MIN, MAXBYTES_LARGEFLOW_MAX)
        else:
            rate = np.random.uniform(RATE_SMALLFLOW_MIN, RATE_SMALLFLOW_MAX)
            maxbytes = np.random.uniform(MAXBYTES_SMALLFLOW_MIN, MAXBYTES_SMALLFLOW_MAX)

        myEvent['rate'] = rate
        myEvent['maxbytes'] = maxbytes

        # Decide arrival time
        arrival_time = 0.0
        arrival_time = np.random.exponential(MEAN_INTER_ARRIVAL)

        return evTime+arrival_time, myEvent




    def gen_init_flows(self, events):
        """
        Generate initial flows before simulation starts
        - For each host in self.hosts, generate a random-dest flow.
        """

        # Generate one flow arrival for each host
        for src in self.hosts.keys():
            arrival_time, myEvent = self.gen_a_rand_dst_flow(src, evTime=0.0)
            heappush(events, (arrival_time, myEvent) )

        return events




    def gen_new_flow(self, src_ip, evTime=0.0):
        """
        Generate new flow. With source specified, we will randomly pick
        a destination from self.hosts. Then we generate either a large
        or small flow based on a Bernoulli result. Then we decide the
        data rate and total bytes of the flow.
        """
        arrival_time, myEvent = self.gen_a_rand_dst_flow(src_ip, evTime)

        return arrival_time, myEvent




    def flow_arrival(self, evTime, evDict_FlowArrival):
        """
        Upon flow arrival, register the flow at self.flows
        """
        src_ip = evDict_FlowArrival['src_ip']
        dst_ip = evDict_FlowArrival['dst_ip']
        src_node = evDict_FlowArrival['src_node']
        dst_node = evDict_FlowArrival['dst_node']
        rate = evDict_FlowArrival['rate']
        maxbytes = evDict_FlowArrival['maxbytes']
        flowid = evDict_FlowArrival['flowid']

        myFlow = (src_ip, dst_ip)

        if ( myFlow in self.flows ):
            self.flows[myFlow]['maxbytes'] += maxbytes
            self.flows[myFlow]['resbytes'] += maxbytes
            self.flows[myFlow]['rate'] += rate

        else:
            self.flows[myFlow] = {}
            self.flows[myFlow]['src_node'] = src_node
            self.flows[myFlow]['dst_node'] = dst_node
            self.flows[myFlow]['rate'] = rate
            self.flows[myFlow]['maxbytes'] = maxbytes
            self.flows[myFlow]['resbytes'] = maxbytes
            self.flows[myFlow]['last_update'] = evTime
            self.flows[myFlow]['status'] = 'requesting'
            self.flows[myFlow]['path'] = []
            self.flows[myFlow]['flowid'] = flowid
            self.flows[myFlow]['bytes_in_slot'] = 0.0
            self.flows[myFlow]['ending_in_slot'] = 0
            self.flows[myFlow]['arrival_time'] = evTime
            self.flows[myFlow]['rerouted'] = 0
            self.flows[myFlow]['rejected'] = 0




    def links_flow_inst(self, evTime, evDict_FlowInst):
        """
        When the simulator sees a FlowInst event, it knows that
        the flow will activate, running on specified path. Therefore its
        flow entry must be installed to all links along the path.
        """
        src_ip = evDict_FlowInst['src_ip']
        dst_ip = evDict_FlowInst['dst_ip']
        path = evDict_FlowInst['path']
        # Update the flow's status to 'active'
        if ( (src_ip, dst_ip) in self.flows ):
            self.flows[(src_ip, dst_ip)]['status'] = 'active'
            self.flows[(src_ip, dst_ip)]['path'] = path
            self.flows[(src_ip, dst_ip)]['last_update'] = evTime
            self.flows[(src_ip, dst_ip)]['st_time'] = evTime
        # Register the flow at links along the path
        for i in range(len(path)-1):
            # To correct the tuple order...
            myA = path[i]
            myB = path[i+1]
            if ( (myA, myB) in self.links ):
                self.links[(myA, myB)]['flows'].append((src_ip, dst_ip))
            else:
                self.links[(myB, myA)]['flows'].append((src_ip, dst_ip))




    def links_flow_end(self, evTime, evDict_FlowEnd):
        """
        On the contrary from links_flow_inst(), when the flow comes to an end,
        its entry must be removed from all links along the path.
        """
        src_ip = evDict_FlowEnd['src_ip']
        dst_ip = evDict_FlowEnd['dst_ip']
        path = self.flows[(src_ip, dst_ip)]['path']
        # Remove flow entry from links along the path
        for i in range(len(path)-1):
            myA = path[i]
            myB = path[i+1]
            if ( (myA, myB) in self.links ):
                self.links[(myA, myB)]['flows'].remove((src_ip, dst_ip))
            else:
                self.links[(myB, myA)]['flows'].remove((src_ip, dst_ip))
        # Mark as inactive in self.flows
        self.flows[(src_ip, dst_ip)]['status'] = 'inactive'




    def flow_idle_timeout(self, evTime, evDict_IdleTimeout):
        src_ip = evDict_IdleTimeout['src_ip']
        dst_ip = evDict_IdleTimeout['dst_ip']
        del self.flows[(src_ip, dst_ip)]




    def flow_stats_update(self, evTime, ctrl, events):
        """
        Update flow stats
        - First calculate how much data can a flow transmit during current window
          (window length = FLOW_STATS_PERIOD).
        - If congestion occurs, starting from the most congested link, reduce # of
          bytes transmitted by involved flows. Iterate over all links until all of
          them are non-congesting.
        """
        # Reset flow stats
        for fl in self.flows:
            self.flows[fl]['bytes_in_slot'] = 0.0
            self.flows[fl]['ending_in_slot'] = 0

        # Estimate each flow's bytes-in-slot
        for fl in self.flows:
            non_counted_time = (evTime+TIME_RES) - self.flows[fl]['last_update']
            if(self.flows[fl]['status'] == 'active'):
                self.flows[fl]['bytes_in_slot'] +=  \
                            self.flows[fl]['rate'] * non_counted_time
                if( self.flows[fl]['bytes_in_slot'] >= self.flows[fl]['resbytes']):
                    self.flows[fl]['bytes_in_slot'] = self.flows[fl]['resbytes']

        # Estimate link utilization (before congestion detection)
        for lk in self.links:
            myFlows = self.links[lk]['flows']
            self.links[lk]['bytes_in_slot'] = sum([self.flows[fl]['bytes_in_slot'] \
                                                  for fl in myFlows])
            self.links[lk]['util_in_slot'] = self.links[lk]['bytes_in_slot'] / \
                                             self.links[lk]['cap_in_slot']

        sorted_link_list = sorted(self.links.keys(), \
                                  key=lambda lk: self.links[lk]['util_in_slot'], \
                                  reverse=True)


        # Congestion handling: Reduce flow transmission bytes if necessary
        curr_link = sorted_link_list[0]
        while (self.links[curr_link]['util_in_slot'] >= 1.0):
            if (SHOW_LINK_CONGESTION > 0):
                print "Congestion at %s: %.6f" %(curr_link, self.links[curr_link]['util_in_slot'])
            myFlows = self.links[curr_link]['flows']
            cong_util = self.links[curr_link]['util_in_slot']
            for fl in myFlows:
                self.flows[fl]['bytes_in_slot'] /= cong_util

            # After reducing flows' bytes_in_slot, update all link's bytes_in_slot
            # and util_in_slot
            for lk in self.links:
                myFlows = self.links[lk]['flows']
                self.links[lk]['bytes_in_slot'] = sum([self.flows[fl]['bytes_in_slot'] \
                                                       for fl in myFlows])
                self.links[lk]['util_in_slot'] = self.links[lk]['bytes_in_slot'] / \
                                                 self.links[lk]['cap_in_slot']

            #print "Resolved", curr_link, self.links[curr_link]['util_in_slot']
            sorted_link_list.remove(curr_link)
            sorted_link_list = sorted(sorted_link_list, \
                                      key=lambda lk: self.links[lk]['util_in_slot'], \
                                      reverse=True)
            curr_link = sorted_link_list[0]


        # Commit the revised flow stats
        for fl in self.flows:
            self.flows[fl]['resbytes'] -= self.flows[fl]['bytes_in_slot']
            #self.flows[fl]['rate'] = self.flows[fl]['bytes_in_slot'] / TIME_RES
            if(self.flows[fl]['resbytes'] <= 0.0):
                self.flows[fl]['ending_in_slot'] = 1

        # Update switch states
        for fl in self.flows:
            myPath = self.flows[fl]['path']
            for mySW in myPath:
                ctrl.switches[mySW][fl]['byte_count'] = self.flows[fl]['bytes_in_slot']

        # Detect flow ends & place idle timeouts.
        for fl in self.flows:
            if(self.flows[fl]['status'] == 'active' and \
               self.flows[fl]['ending_in_slot'] == 1):
                # Estimate FlowEnd time
                flowend_time = self.flows[fl]['last_update'] + \
                               self.flows[fl]['bytes_in_slot'] / self.flows[fl]['rate']
                # Schedule a FlowEnd event
                evDict = copy_dict(self.flows[fl], 'src_node', 'dst_node', \
                                   'flowid', 'path')
                evDict['type'] = 'FlowEnd'
                evDict['src_ip'] = fl[0]
                evDict['dst_ip'] = fl[1]
                heappush(events, (flowend_time, evDict))

        # Possibly log some flow stats here
        # Possibly log some link stats here
        # Possibly log some switch stats here

        # Reset stats
        for fl in self.flows:
            self.flows[fl]['last_update'] = evTime




    def flow_stats_update_maxmin(self, evTime, ctrl, events):
            """
            Update flow statistics using max-min bandwidth sharing model.
            Please refer to:
            [1] Q. Ma, P. Steenkiste and H. Zhang, "Routing High-bandwidth Traffic in
                Max-min Fair Share Networks," in SIGCOMM '96
            """

            # Initialization: set lists sat_flows = NULL and unsat_flows = all flows.
            sat_flows = []
            unsat_flows = [fl for fl in self.flows if self.flows[fl]['status'] == 'active']
            for fl in unsat_flows:
                self.flows[fl]['bytes_in_slot'] = 0.0       # Reset
                self.flows[fl]['ending_in_slot'] = 0        # Reset
            for lk in self.links:
                self.links[lk]['assigned_bw'] = 0.0

            # Repeat until there is no unsaturated flows in the network
            while ( len(unsat_flows) > 0 ):
                # Build a list of links that unsaturated flows run on
                links_of_unsat = []
                for fl in unsat_flows:
                    myLinks = getedges(self.flows[fl]['path'], self)
                    for lk in myLinks:
                        if (not lk in links_of_unsat):
                            links_of_unsat.append(lk)

                # Calculate each link's incremental BW
                flows_to_increment = set()
                min_inc = np.inf  # Just a large number as initial
                most_congested_link = ''

                for lk in links_of_unsat:
                    sat_lk = set(sat_flows) & set(self.links[lk]['flows'])
                    unsat_lk = set(unsat_flows) & set(self.links[lk]['flows'])
                    cap = self.links[lk]['cap_in_slot']
                    assigned_bw = self.links[lk]['assigned_bw']
                    #sat_lk_bw = sum( [self.flows[fl]['bytes_in_slot'] for fl in sat_lk] )
                    #unsat_lk_bw = sum( [self.flows[fl]['bytes_in_slot'] for fl in unsat_lk] )
                    #inc_bw = (cap - sat_lk_bw - unsat_lk_bw) / len(unsat_lk)
                    inc_bw = (cap - assigned_bw) / len(unsat_lk)

                    if (SHOW_FLOW_CALC > 0):
                        print lk, cap, sat_lk_bw, len(sat_lk), len(unsat_lk), inc_bw

                    if (inc_bw < min_inc):
                        min_inc = inc_bw
                        most_congested_link = lk
                        flows_to_increment = unsat_lk

                if (SHOW_FLOW_CALC > 0):
                    print most_congested_link


                # Commit BW increment to flows_to_increment
                still_some_bw = 0
                temp_flows =[]

                for fl in flows_to_increment:
                    bytes_in_slot = self.flows[fl]['bytes_in_slot']
                    resbytes = self.flows[fl]['resbytes']
                    allowed_bytes = bytes_in_slot + min_inc
                    links_fl = getedges(self.flows[fl]['path'], self)

                    if (SHOW_FLOW_CALC > 0):
                        print fl, bytes_in_slot, resbytes, min_inc,

                    if (allowed_bytes < resbytes):
                        if (SHOW_FLOW_CALC > 0):
                            print "Not yet saturated...",
                        self.flows[fl]['bytes_in_slot'] += min_inc
                        for lk in links_fl:
                            self.links[lk]['assigned_bw'] += min_inc
                        self.flows[fl]['allowed_bytes'] = allowed_bytes
                        temp_flows.append(fl)       # Not yet decided as saturated flow

                    elif (allowed_bytes == resbytes):
                        inc_temp = resbytes - self.flows[fl]['bytes_in_slot']
                        if (SHOW_FLOW_CALC > 0):
                            print "Saturated. More to transmit.",
                        self.flows[fl]['bytes_in_slot'] = resbytes
                        for lk in links_fl:
                            self.links[lk]['assigned_bw'] += inc_temp
                        self.flows[fl]['allowed_bytes'] = allowed_bytes
                        #self.flows[fl]['ending_in_slot'] = 1
                        unsat_flows.remove(fl)
                        sat_flows.append(fl)

                    elif (allowed_bytes > resbytes):
                        inc_temp = resbytes - self.flows[fl]['bytes_in_slot']
                        if (SHOW_FLOW_CALC > 0):
                            print "Saturated. Will finish.",
                        self.flows[fl]['bytes_in_slot'] = resbytes
                        for lk in links_fl:
                            self.links[lk]['assigned_bw'] += inc_temp
                        self.flows[fl]['allowed_bytes'] = allowed_bytes
                        unsat_flows.remove(fl)
                        sat_flows.append(fl)
                        still_some_bw = 1

                    if (SHOW_FLOW_CALC > 0):
                        print self.flows[fl]['bytes_in_slot']


                if (still_some_bw == 0):
                    # If lk has no BW left, move all flows running on it
                    # to sat_flows list.
                    if (SHOW_FLOW_CALC > 0):
                        print most_congested_link, "used up"

                    for fl in temp_flows:
                        unsat_flows.remove(fl)
                        sat_flows.append(fl)

                #raw_input()

            # Commit link stats
            for lk in self.links:
                self.links[lk]['bytes_in_slot'] = sum([self.flows[fl]['bytes_in_slot']  \
                                                       for fl in self.links[lk]['flows'] if
                                                       self.flows[fl]['status'] == 'active'])
                self.links[lk]['util_in_slot'] = self.links[lk]['bytes_in_slot'] / \
                                                 self.links[lk]['cap_in_slot']


            # Commit the revised flow stats
            for fl in self.flows:
                self.flows[fl]['resbytes'] -= self.flows[fl]['bytes_in_slot']
                if(self.flows[fl]['resbytes'] <= 0.0):
                    self.flows[fl]['ending_in_slot'] = 1

            # Update switch states
            for fl in self.flows:
                myPath = self.flows[fl]['path']
                for mySW in myPath:
                    ctrl.switches[mySW][fl]['byte_count'] = self.flows[fl]['bytes_in_slot']

            # Detect flow ends & place idle timeouts.
            for fl in self.flows:
                if(self.flows[fl]['status'] == 'active' and \
                   self.flows[fl]['ending_in_slot'] == 1):
                    # Estimate FlowEnd time
                    flowend_time = self.flows[fl]['last_update'] + \
                                   self.flows[fl]['bytes_in_slot'] /  \
                                   self.flows[fl]['allowed_bytes'] * TIME_RES
                    # Schedule a FlowEnd event
                    evDict = copy_dict(self.flows[fl], 'src_node', 'dst_node', \
                                       'flowid', 'path')
                    evDict['type'] = 'FlowEnd'
                    evDict['src_ip'] = fl[0]
                    evDict['dst_ip'] = fl[1]
                    heappush(events, (flowend_time, evDict))

            for fl in self.flows:
                self.flows[fl]['last_update'] = evTime


