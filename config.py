# --------------------------------------------------------------------------------
# config.py
#
# Flow-level simulator for link utilization and flow table usage
#
# File Description: Parameters for simulator
#
# Author: Kuan-yin Chen
# Mail: cgi0911@gmail.com
# NYU Polytechnic School of Engineering
# --------------------------------------------------------------------------------

import os

# ----------------------------------------
# Frequently used
# ----------------------------------------
#DIR_LOG = './sdar_k1_noreroute'
K_PATH = 1                 # No. of paths for predefined for each src/dst pair
                            # Setting K_PATH to 1 equals to shortest path first
DO_REROUTE = 0              # Do elephant flow rerouting (please refer to paper draft)
MODE_PATH_SEL = 'table_aware'
TABLE_SIZE_PER_SW = 400     # Table size per switch
N_HOSTS_PER_SW = 15         # Hosts per switch

SHOW_PROGRESS = 1           # Be verbose 




# ----------------------------------------
# File names
# ----------------------------------------

TOP_DIR = '/home/cgi0911/Workspace/Flowlevel/flowlevel'
FN_NODES = os.path.join(TOP_DIR, 'Topologies/sprint.nodes.csv')
FN_LINKS = os.path.join(TOP_DIR, 'Topologies/sprint.links.csv')
#DIR_LOG = './sdar_k2_reroute'
FN_EXT1 = '.csv'
FN_EXT2 = '.txt'
FN_LOG_LINK_UTIL = 'log_link_util'
FN_LOG_TABLE_UTIL = 'log_table_util'
FN_LOG_FLOW_STATS = 'log_flow_stats'
FN_PARAMS = 'log_params'
FN_SUMMARIES = 'summaries'
    # See the end of this script for filename creation


# ----------------------------------------
# Logging
# ----------------------------------------

#SHOW_PROGRESS = 1
SHOW_EVENTS = 0
SHOW_K_PATH_CONSTRUCTION = 0
SHOW_LINK_CONGESTION = 0
SHOW_TABLE_OVERFLOW = 0
SHOW_FLOW_CALC = 0
SHOW_REROUTE = 0

LOG_PARAMS = 1
LOG_LINK_UTIL = 1
LOG_TABLE_UTIL =  1
LOG_FLOW_STATS = 1
LOG_SUMMARIES = 1



# ----------------------------------------
# Operation modes
# ----------------------------------------
#MODE_PATH_SEL = 'table_aware'
                            # Path selection mode
                            # 'table_aware' - try to balance table util. when instsalling a
                            #                 new flow.
                            # 'random' - randomly choose a path from k-paths.
                            # 'shortest' - select the shortest path from src to dst.
K_PATH_METHOD = 'yen'       # Algorithm used for k-path routing
                            # 'yen' - use Yen's algorithm
                            # 'constrained' - exhaustive search of all paths
                            #                 no more than RELAX hops from shortest distance
                            # 'shortest_only' - default mode. accepts only shortest k-paths

#K_PATH = 2                  # No. of paths for predefined for each src/dst pair
RELAX = 8                   # Relax hop counts (for constrained path method)

#DO_REROUTE = 1              # Do elephant flow rerouting (please refer to paper draft)
HH_THRESH = 0.025            # If over this threshold, report as HH.




# ----------------------------------------
# Time-related parameters
# ----------------------------------------

SIM_TIME = 30.0             # Total simulation time
TIME_RES = 0.1              # Simulation time resolution

HH_REROUTE_PERIOD = 1.0     # Heavy hitter report period
FLOW_STATS_PERIOD = 0.1     # Flow stats re-calculation period
SW_CTRL_DELAY = 0.005       # Switch-to-controller delay
CTRL_SW_DELAY = 0.005       # Controller-to-switch delay
IDLE_TIMEOUT = 0.0          # Idle (soft) timeout of flow entry
HARD_TIMEOUT = 20.0         # Hard timeout of flow entry
REJECT_TIMEOUT = 0.100      # Timeout for flow re-request if rejected due to overflow

ELIM_SLOTS = 50             # When doing summary, eliminate head slots because unstable




# ----------------------------------------
# Flow generator-related parameters
# ----------------------------------------

PROB_LARGEFLOW = 0.1        # Probablility that a large flow is generated
                            # We use Bernoulli to decide large or small flow:
                            # PROB_SMALLFLOW = 1 - PROB_LARGEFLOW
PROB_SMALLFLOW = 1.0 - PROB_LARGEFLOW

# We use Uniform Random to decide flow data rate and max bytes.
RATE_LARGEFLOW_MIN = 1e9
RATE_LARGEFLOW_MAX = 1e9
MAXBYTES_LARGEFLOW_MIN = 0.5e8
MAXBYTES_LARGEFLOW_MAX = 4.5e8

RATE_SMALLFLOW_MIN = 50e6
RATE_SMALLFLOW_MAX = 50e6
MAXBYTES_SMALLFLOW_MIN = 50e3
MAXBYTES_SMALLFLOW_MAX = 50e3

# We use Exponential Distribution to decide inter-arrival time until next flow comes.
MEAN_INTER_ARRIVAL = 0.001    # In seconds

MAX_FLOWRATE = 1e9



# ----------------------------------------
# Processing filenames
# ----------------------------------------
if (MODE_PATH_SEL == 'shortest' and DO_REROUTE == 1):
    DIR_LOG = os.path.join(TOP_DIR, 'star_k%d_te' %(K_PATH) )
elif (MODE_PATH_SEL == 'table_aware'):
    DIR_LOG = os.path.join(TOP_DIR, 'star_k%d_reroute%d' %(K_PATH, DO_REROUTE))
else:
    DIR_LOG = os.path.join(TOP_DIR, 'star_temp')


# ---------- Automatically rename files if already exists ----------
i1, i2, i3, i4 = 0, 0, 0, 0
while os.path.exists( os.path.join( DIR_LOG, FN_LOG_LINK_UTIL + '_' + str(i1) + FN_EXT1 ) ):
    i1 += 1
while os.path.exists( os.path.join( DIR_LOG, FN_LOG_TABLE_UTIL + '_' + str(i2) + FN_EXT1 ) ):
    i2 += 1
while os.path.exists( os.path.join( DIR_LOG, FN_LOG_FLOW_STATS + '_' + str(i3) + FN_EXT1 ) ):
    i3 += 1
while os.path.exists( os.path.join( DIR_LOG, FN_PARAMS + '_' + str(i4) + FN_EXT2 ) ):
    i4 += 1

FN_LOG_LINK_UTIL_FULL   = os.path.join(DIR_LOG, FN_LOG_LINK_UTIL + '_' + str(i1) + FN_EXT1)
FN_LOG_TABLE_UTIL_FULL  = os.path.join(DIR_LOG, FN_LOG_TABLE_UTIL + '_' + str(i2) + FN_EXT1)
FN_LOG_FLOW_STATS_FULL  = os.path.join(DIR_LOG, FN_LOG_FLOW_STATS + '_' + str(i3) + FN_EXT1)
FN_PARAMS_FULL          = os.path.join(DIR_LOG, FN_PARAMS + '_' + str(i4) + FN_EXT2)
FN_SUMMARIES_FULL       = os.path.join(DIR_LOG, FN_SUMMARIES + FN_EXT2)     # Will append to same file.


