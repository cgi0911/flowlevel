# --------------------------------------------------------------------------------
# config.py
#
# Flow-level simulator for link utilization and flow table usage
#
# File Description: Functions for creating events
#
# Author: Kuan-yin Chen
# Mail: cgi0911@gmail.com
# NYU Polytechnic School of Engineering
# --------------------------------------------------------------------------------

from config import *
from heapq import *
import os
import numpy as np
import pprint as pp

def gettime(event_item):
    "Return the time (in seconds) of the event item."
    return event_item[0]




def getdict(event_item):
    "Return the dict content of the event item."
    return event_item[1]




def copy_dict(d, *keys):
    """Make a copy of only the specified keys from dictionary d."""
    return {key: d[key] for key in keys}




def rmse(myList):
    myMean = np.mean(myList)
    sum_sqerr = sum( map(lambda n: (n-myMean)**2, myList) )
    mean_sqerr = sum_sqerr / len(myList)
    #result = np.sqrt(mean_sqerr) / myMean
    result = np.sqrt(mean_sqerr)
    return result




def log_params():
    os.system("cp %s %s" %(os.path.join(TOP_DIR, 'config.py'), FN_PARAMS_FULL))
    return




def handle_FlowArrival_event(evTime, evDict_FlowArrival, flowgen):
    """
    Handling FlowArrival event.
    - Register the flow entry and mark as 'requesting' at flowgen.
    - Create a PacketIn event
    """
    # Register flow entry
    flowgen.flow_arrival(evTime, evDict_FlowArrival)
    evDict = copy_dict(evDict_FlowArrival, \
                       'src_ip', 'dst_ip', 'src_node', 'dst_node', 'flowid')
    evDict['type'] = 'PacketIn'
    return evTime+SW_CTRL_DELAY, evDict




def handle_PacketIn_event(evTime, evDict_PacketIn, ctrl):
    """
    Handling PacketIn event.
    - Create a FlowInst event. Must involve controller to compute the path.
    """
    evDict = copy_dict(evDict_PacketIn, \
                       'src_ip', 'dst_ip', 'src_node', 'dst_node', 'flowid')
    evDict['type'] = 'FlowInst'
    src_node = evDict['src_node']
    dst_node = evDict['dst_node']

    if (MODE_PATH_SEL == 'table_aware'):
        evDict['path'] = ctrl.path_sel_table_aware(src_node, dst_node)
    elif (MODE_PATH_SEL == 'random'):
        evDict['path'] = ctrl.path_sel_random(src_node, dst_node)
    else:
        evDict['path'] = ctrl.path_sel_shortest(src_node, dst_node)

    if (evDict['path'] == []):
        # If no available path, reject this PacketIn and resend after REJECT_TIMEOUT
        return evTime+REJECT_TIMEOUT, evDict_PacketIn
    else:
        return evTime+CTRL_SW_DELAY, evDict




def handle_FlowInst_event(evTime, evDict_FlowInst, ctrl, flowgen, events):
    """
    Handling FlowInst event.
    - Install the flow entry to switches on the path
    - Register the flow to flowgen
    - TEMPORARY: Assume there is no congestion and all flows are running at data rate.
    """
    myFlow = (evDict_FlowInst['src_ip'], evDict_FlowInst['dst_ip'])
    myPath = evDict_FlowInst['path']

    # Sanity check: check if path has flow table overflow
    path_overflow = 0
    for sw in myPath:
        if ( len(ctrl.switches[sw]) >= ctrl.topo.node[sw]['table_size'] ):
            path_overflow = 1
            break

    if (path_overflow > 0):
        # Silently discard flow installation
        if (SHOW_TABLE_OVERFLOW > 0):
            print myFlow, "discarded due to overflow at installation."
        #del flowgen.flows[myFlow]
        # Resend PacketIn after REJECT_TIMEOUT
        evDict = copy_dict(evDict_FlowInst, \
                           'src_ip', 'dst_ip', 'src_node', 'dst_node', 'flowid')
        evDict['type'] = 'PacketIn'
        return 1, evDict
    else:
        # Install flow entry to switches on the path
        ctrl.switches_flow_inst(evTime, evDict_FlowInst)
        # Register the flow to flowgen
        flowgen.links_flow_inst(evTime, evDict_FlowInst)
        return 0, {}





def handle_FlowEnd_event(evTime, evDict_FlowEnd, ctrl, flowgen, f_file):
    """
    Handling FlowEnd event
    - De-register the flow entry from links along the path
    - Schedule a IdleTimeout event
    """
    myFlow = (evDict_FlowEnd['src_ip'], evDict_FlowEnd['dst_ip'])
    myDict = flowgen.flows[myFlow]
    myDict['ed_time'] = evTime
    # Log flow stats if enabled
    if (LOG_FLOW_STATS > 0):
        f_file.write('%s,%s,' %(str(myFlow[0]), str(myFlow[1]) ) )
        f_file.write('%s,%s,' %(myDict['src_node'], myDict['dst_node']) )
        f_file.write('%d,' %(myDict['flowid']))
        f_file.write('%.10f,' %(myDict['maxbytes']))
        f_file.write('%d,' %(myDict['rerouted']))
        f_file.write('%.10f,' %(myDict['arrival_time']))
        f_file.write('%.10f,' %(myDict['st_time']))
        f_file.write('%.10f,' %(myDict['ed_time']))
        flow_time = myDict['ed_time'] - myDict['st_time']
        f_file.write('%.10f,' %(flow_time))
        f_file.write('%.10f' %( myDict['maxbytes'] / flow_time ))
        f_file.write('\n')


    # Remove entries from links
    flowgen.links_flow_end(evTime, evDict_FlowEnd)
    # Schedule an idle timeout event
    evDict = {}
    evDict = copy_dict(evDict_FlowEnd, 'src_ip', 'dst_ip', 'src_node', 'dst_node', \
                       'flowid', 'path')
    evDict['type'] = 'IdleTimeout'

    return evTime+IDLE_TIMEOUT, evDict




def handle_IdleTimeout_event(evTime, evDict_IdleTimeout, ctrl, flowgen):
    """
    Handling IdleTimeout event
    - Remove flow entry from switches along the path
    """
    ctrl.switches_idle_timeout(evTime, evDict_IdleTimeout, flowgen)
    flowgen.flow_idle_timeout(evTime, evDict_IdleTimeout)

    src_ip = evDict_IdleTimeout['src_ip']
    dst_ip = evDict_IdleTimeout['dst_ip']
    myFlow = (src_ip, dst_ip)

    if ( (myFlow) in ctrl.hh_flows ):
        del ctrl.hh_flows[myFlow]

    if ( (myFlow) in ctrl.new_hh_flows ):
        del ctrl.new_hh_flows[myFlow]




def handle_HHReport_event(evTime, evDict_HHReport = {}):
    "Create a heavy hitter report event"
    eventDict = {}
    eventDict['type'] = 'HHReport'
    return evTime+HH_REPORT_PERIOD, eventDict




def update_stats(sys_time, ctrl, flowgen, events):
    """
    Handling FlowStats events
    - First calculate how much data can a flow transmit during current window
      (window length = FLOW_STATS_PERIOD).
    - If congestion occurs, starting from the most congested link, reduce # of
      bytes transmitted by involved flows. Iterate over all links until all of
      them are non-congesting.
    - Update flow counters of each flow at involved switches.
    - Reset flowgen.flows['slotbytes'] and other flags.
    !!! Currently not supporting hard timeout !!!
    """
    #flowgen.flow_stats_update(sys_time, ctrl, events)
    flowgen.flow_stats_update_maxmin(sys_time, ctrl, events)




def getedges(path, flowgen):
    """
    Given a path (represented in an ordered list of nodes), return a list
    of involved edges. Note that each edge, represented in an ordered tuple,
    shall be consistent to ctrl.topo
    """
    ret = []
    for i in range( len(path)-1 ):
        if ( (path[i], path[i+1]) in flowgen.links ):
            ret.append( (path[i], path[i+1]) )
        elif ( (path[i+1], path[i]) in flowgen.links ):
            ret.append( (path[i+1], path[i]) )
    return ret




def comB(ctrl, flowgen):
    """
    Given a set of (old) big flows, iteratively compute their max-min BW allocation
    in the given topology & link capacities.
    """
    if ( len(ctrl.hh_flows) == 0 ):
        return  # No existing HH flow yet.

    myEdges = {}    # key: links, value: residual link capacity
    myPaths = {}    # key: flows, value: its path
    myU = {}        # key: links, value: list of big flows running on it

    for fl in ctrl.hh_flows:
        flPath = flowgen.flows[fl]['path']
        myPaths[fl] = flPath
        for ed in getedges(flPath, flowgen):
            if (not ed in myEdges):
                myEdges[ed] = flowgen.links[ed]['cap_in_slot']
            if (not ed in myU):
                myU[ed] = [fl]
            else:
                myU[ed].append(fl)

    for fl in ctrl.hh_flows:
        ctrl.hh_flows[fl] = 0.0     # Reset

    while ( len(myPaths.keys()) <> 0 ):
        e_g_list = [ee for ee in myEdges.keys() if len(myU[ee]) > 0]
        e_g = sorted( e_g_list, key=lambda e: myEdges[e]/float(len(myU[e]))  )[0]
        b_temp = myEdges[e_g]/float(len(myU[e_g]))
        for a_xt in myU[e_g]:
            ctrl.hh_flows[a_xt] = b_temp
            for ed in getedges(myPaths[a_xt], flowgen):
                myEdges[ed] -= ctrl.hh_flows[a_xt]
                if (myEdges[ed] <= 0.0):
                    del myEdges[ed]
                myU[ed].remove(a_xt)
            del myPaths[a_xt]

    for fl in ctrl.hh_flows:
        flowgen.flows[fl]['rate'] = ctrl.hh_flows[fl] / TIME_RES




def find_widest(myFlow, ctrl, flowgen):
    src_node = flowgen.flows[myFlow]['src_node']
    dst_node = flowgen.flows[myFlow]['dst_node']
    myPaths = ctrl.k_path_db[(src_node, dst_node)]

    oab = {}        # key: all edges in topology, value: observed available BW

    for myEdge in flowgen.links:
        oab[myEdge] = flowgen.links[myEdge]['cap_in_slot']  # Set to full capacity
                                                            # initially.

    myU = {}        # key: links, value: list of big flows running on it

    for fl in ctrl.hh_flows:
        flPath = flowgen.flows[fl]['path']
        for ed in getedges(flPath, flowgen):
            if (not ed in myU):
                myU[ed] = [fl]
            else:
                myU[ed].append(fl)

    for ee in oab:
        if (not ee in myU):
            continue            # ee is not used by any flow in ctrl.hh_flows

        a_tilda = []    # Please see paper draft.

        for fl1 in myU[ee]:
            a_hat = [fl2 for fl2 in myU[ee] if \
                     (fl2 <> fl1) and (ctrl.hh_flows[fl2] < ctrl.hh_flows[fl1])  ]

            a_hat_total_bw = sum( [ ctrl.hh_flows[fl3] for fl3 in a_hat ] )
            a_hat_size = len(a_hat)

            original_cap = flowgen.links[ee]['cap_in_slot']
            if (ctrl.hh_flows[fl1] >= (original_cap - a_hat_total_bw) / \
                                      float(len(myU[ee]) - a_hat_size + 1)  ):
                a_tilda.append(fl1)

        a_tilda_reverse = [fl4 for fl4 in myU[ee] if (not fl4 in a_tilda)]
        a_tilda_reverse_total_bw = sum( [ctrl.hh_flows[fl5] for fl5 in a_tilda_reverse] )
        oab[ee] = (original_cap - a_tilda_reverse_total_bw) / \
                  float( len(a_tilda) + 1)

    # Now pick the widest path
    curr_bw = 0.0
    best_path = []
    for i in range(len(myPaths)):
        myPath = myPaths[i]

        # Check if flow table is full. Skip if full.
        path_overflow = 0
        for sw in myPath:
            if ( len(ctrl.switches[sw]) >= ctrl.topo.node[sw]['table_size'] ):
                path_overflow = 1
                break
        if (path_overflow > 0):
            continue

        btneck_bw = min( [oab[ed] for ed in getedges(myPath, flowgen)]  )

        if (btneck_bw > curr_bw):
            best_path = myPath
            curr_bw = btneck_bw

    return best_path, curr_bw




def do_reroute(evTime, ctrl, flowgen, events):
    """
    Reroute elephant FlowStats
    - First pull HH reports from each link (per-link, not per-switch), using HH_THRESH.
    - Without considering small flows, do greedy algorithm to allocate BW of big flows.
      (refer to paper draft). The big flows' flowgen.flows[fl]['rate'] will be updated.
    - Assume the original flow is immediately terminated, and the new-routed flow will
      be scheduled to arrive immediately, so will be installed after SW_CTRL_DELAY.
    - If a flow is ended, remove it from ctrl.hh_flows.
    """
    # Pull HH reports
    ctrl.new_hh_flows = {}
    for lk in flowgen.links:
        for fl in flowgen.links[lk]['flows']:
            if ( flowgen.flows[fl]['bytes_in_slot'] >= \
                 flowgen.links[lk]['cap_in_slot'] * HH_THRESH and
                 not fl in ctrl.hh_flows and
                 flowgen.flows[fl]['status'] == 'active' ):
                #print fl, "is a HH. bytes_in_slot =", flowgen.flows[fl]['bytes_in_slot'], "maxbytes =", flowgen.flows[fl]['maxbytes']
                ctrl.new_hh_flows[fl] = flowgen.flows[fl]['bytes_in_slot']

    #ctrl.hh_flows = {}

    # Find routes for ctrl.new_hh_flows
    to_reroute_flows = {}       # key: flows in new_hh_flows, value: widest path
    while ( len(ctrl.new_hh_flows) > 0 ):
        comB(ctrl, flowgen)     # Find BW allocation for each active large flow

        myFlow = ctrl.new_hh_flows.keys()[0]    # Pick a flow from ctrl.new_hh_flows

        myPath, oab = find_widest(myFlow, ctrl, flowgen)     # Find widest path i.t.o. OAB

        to_reroute_flows[myFlow] = myPath
        ctrl.hh_flows[myFlow] = oab

        del ctrl.new_hh_flows[myFlow]   # Remove myFlow from new_hh_flows after finding route

    # ------------------------------
    # Commit the re-routing
    # ------------------------------
    for myFlow in to_reroute_flows:
        original_path = flowgen.flows[myFlow]['path']
        new_path = to_reroute_flows[myFlow]

        # ---------- New path is the same as old one ----------
        if (new_path == original_path):
            if (SHOW_REROUTE > 0):
                print myFlow, "no need to reroute."
            continue

        # ---------- No available path due to table overflow ----------
        if (new_path == []):
            if (SHOW_REROUTE > 0):
                print myFlow, "No path available with flow table space. No reroute."
            continue

        if (SHOW_REROUTE > 0):
            print myFlow, original_path, "->", new_path, flowgen.flows[myFlow]['bytes_in_slot']

        flowgen.flows[myFlow]['rerouted'] += 1          # Update reroute counter

        # Remove old switch entires
        for mySW in original_path:
            del ctrl.switches[mySW][myFlow]

        # Update the path's switches
        flowgen.flows[myFlow]['path'] = new_path
        ctrl.flows[myFlow]['path'] = new_path

        for i in range(len(new_path)):
            mySW = new_path[i]
            ctrl.switches[mySW][myFlow] = {}
            ctrl.switches[mySW][myFlow]['src_ip'] = myFlow[0]
            ctrl.switches[mySW][myFlow]['dst_ip'] = myFlow[1]

            if (i == 0):
                ctrl.switches[mySW][myFlow]['last_hop'] = myFlow[0]
            else:
                ctrl.switches[mySW][myFlow]['last_hop'] = new_path[i-1]

            if ( i == len(new_path)-1 ):
                ctrl.switches[mySW][myFlow]['next_hop'] = myFlow[1]
            else:
                ctrl.switches[mySW][myFlow]['next_hop'] = new_path[i+1]

            ctrl.switches[mySW][myFlow]['byte_count'] = 0.0

        # Update the path's links
        for ed in getedges(original_path, flowgen):
            if (myFlow in flowgen.links[ed]['flows']):
                flowgen.links[ed]['flows'].remove(myFlow)

        for ed in getedges(new_path, flowgen):
            if (not myFlow in flowgen.links[ed]['flows']):
                flowgen.links[ed]['flows'].append(myFlow)

        # Update the trasmission rate
        # flowgen.flows[myFlow]['rate'] = MAX_FLOWRATE

